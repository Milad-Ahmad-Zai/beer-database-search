import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Beer } from '../interfaces/beer';

@Injectable({
  providedIn: "root"
})

export class PunkApiService {
  BASE_URL = "https://api.punkapi.com/v2/";
  PAGE = 1;
  PER_PAGE = 10;

  beers = new BehaviorSubject<Beer[]>(null);
  cast = this.beers.asObservable();

  constructor(private http: HttpClient) {}

  /**
 * @param {page} {perpage}  Page number and items per page
 *
 * @example
 * service.getBeers(1, 20) Return Page 1 with 20 Items
 *
 * @returns List of beers
 */
  getBeers(){
    return this.http.get(`${this.BASE_URL}/beers?page=${this.PAGE}&per_page=${this.PER_PAGE}`)
    .pipe(map(data => this.beers.next(<Beer[]>data)));
  }

  getMoreBeers(){
    this.PAGE++;
    return this.http.get(`${this.BASE_URL}/beers?page=${this.PAGE}&per_page=${this.PER_PAGE}`)
    .pipe(map(data => this.beers.next(this.beers.getValue().concat(<Beer>data))));
  }

  getSimilars(abv, ibu, ebc) {
    return this.http.get(`${this.BASE_URL}/beers?per_page=3&abv_gt=${Math.floor(abv)-2}&abv_lt=${Math.ceil(abv)+2}&ibu_gt=${Math.floor(ibu)-10}&ibu_lt=${Math.ceil(ibu)+10}&ebc_gt=${Math.floor(ebc)-10}&ebc_lt=${Math.ceil(ebc)+10}`);
  }

  getBeerDetail(id) {
    return this.http.get(`${this.BASE_URL}/beers/${id}`);
  }

}
