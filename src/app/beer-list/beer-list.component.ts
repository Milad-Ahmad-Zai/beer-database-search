import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router  } from '@angular/router';
import { PunkApiService } from '../services/punkapi.service';
import { Beer } from '../interfaces/beer';
import { BeerDetailComponent } from '../beer-detail/beer-detail.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.scss']
})
export class BeerListComponent implements OnInit {
  public modalRef: BsModalRef;
  public beers: Beer[];

  constructor(private apiService: PunkApiService, private modalService: BsModalService, private route: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService){}

  ngOnInit(){
    this.spinner.show();
    this.route.params.subscribe(params =>{
      if(params.id !== undefined){
        this.getBeerDetail(params.id);
      }else{
        this.getBeers();
      }
    });
    
  }

  /**
   * Get single beer
   */
  getBeerDetail(id){
    this.apiService.getBeerDetail(id).subscribe((data) => this.openModal(data[0]));
  }

  /**
   * Get beers
   */

  public getBeers () {
    this.apiService.getBeers().subscribe(()=>{
      this.apiService.cast.subscribe(
        response => {
          this.spinner.hide();
          this.beers = response;
          console.log(response);
        }
      );
    })
      
  }
  
  /**
   * Load more beers on scroll
   */

  loadMore(){
    this.spinner.show();
    this.apiService.getMoreBeers().subscribe(()=>{
      this.apiService.cast.subscribe(data => {
        this.spinner.hide();
        this.beers = data;
      })
    })
  }

  /**
   * Open beer detail modal
   */
  openModal(selectedBeer: Beer){
    const initialState = { selectedBeer: selectedBeer };
    this.modalRef = this.modalService.show(BeerDetailComponent,  Object.assign({}, {}, { class: 'modal-lg', initialState }));
    this.modalService.onHide.subscribe(()=> this.router.navigate(['/beers']));
    this.spinner.hide();
  }

}
