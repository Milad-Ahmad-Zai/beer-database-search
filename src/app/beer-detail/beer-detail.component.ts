import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Beer } from '../interfaces/beer';
import { PunkApiService } from '../services/punkapi.service';

@Component({
  selector: 'app-beer-detail',
  templateUrl: './beer-detail.component.html',
  styleUrls: ['./beer-detail.component.scss']
})
export class BeerDetailComponent implements OnInit {
  similiarBeers: Beer[] = [];
  @Input() selectedBeer: Beer;

  constructor(private apiService: PunkApiService, private modalRef: BsModalRef ) { }

  ngOnInit() {
    this.getSimiliarBeers();
  }

  /**
   * Get similiar beers
   */
  getSimiliarBeers(){
    this.apiService.getSimilars(this.selectedBeer.abv, this.selectedBeer.ibu, this.selectedBeer.ebc).subscribe((data:Beer[]) => {
      this.similiarBeers = data;
    });
  }

  onSave(){
    this.modal.hide();
  }

}
