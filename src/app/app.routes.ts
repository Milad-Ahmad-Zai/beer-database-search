import {Route} from '@angular/router';
import { BeerListComponent } from './beer-list/beer-list.component';
import { BeerDetailComponent } from './beer-detail/beer-detail.component'

export const routes:Route[] = [
    {path: '', redirectTo: 'beers', pathMatch: 'full'},
    {path: 'beers', component: BeerListComponent},
    {path: 'beers/:id', component: BeerListComponent},
];



























































































































