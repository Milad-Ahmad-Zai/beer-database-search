import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { ModalModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { PunkApiService } from './services/punkapi.service';
import { BeerListComponent } from './beer-list/beer-list.component';
import { BeerDetailComponent } from './beer-detail/beer-detail.component'
import { routes } from './app.routes';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    BeerListComponent,
    BeerDetailComponent
  ],
  entryComponents: [BeerDetailComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [PunkApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
